"""
API extension for stage calibration

This file contains the HTTP API for camera/stage calibration.
It is a plugin for v2 of the openflexure microscope, and is much more interesting
if you use a version of labthings that supports logging for background tasks
"""
from labthings.server.view import View
from labthings.server.find import find_component
from labthings.server.extensions import BaseExtension
from labthings.server.decorators import marshal_task, ThingAction, use_args
from labthings.server import fields

from labthings.core.tasks import taskify
from labthings.core.utilities import get_by_path, set_by_path, create_from_path


from flask import abort, jsonify

import logging
import time


class DummyTaskExtension(BaseExtension):
    """
    Count in a background task, logging as we go.
    """
    def __init__(self):
        BaseExtension.__init__(
            self,
            "org.openflexure.dummy_task",
            version="0.0.1",
        )

def count(N, dt):
    for i in range(N):
        time.sleep(dt)
        logging.info(f"Counted to {i}")

dummy_task_extension = DummyTaskExtension()

@ThingAction
class LogCounterView(View):
    @marshal_task
    @use_args({
        "N": fields.Int(description="The number to count up to", required=False, example=10, missing=10),
        "dt": fields.Float(description="Time to wait between counts", required=False, example=0.1, missing=0.1)
    })
    def post(self, args):
        """Calibrate both axes of the microscope stage against the camera."""
        task = taskify(count)(args.get("N"), args.get("dt"))

        return task

dummy_task_extension.add_view(LogCounterView, "/count")


