from __future__ import print_function, division
import requests
import json
import time

class MicroscopeConsole(object):
    def __init__(self, host, port=5000):
        self.host = host
        self.port = port
        
    @property
    def base_uri(self):
        return "http://{}:{}/api/v1".format(self.host, self.port)
        
    def run_short(self, code):
        """Run a short (blocking) task on the microscope.
        
        The code supplied can put things in ``response`` which is a
        dictionary.  This is available in the ``return`` key of the 
        dictionary that's returned.
        """
        r = requests.post(self.base_uri+"/plugin/console/short_task", 
                          json={"code":code})
        r.raise_for_status()
        return r.json()
        
    def start_background_task(self, code):
        """Start a long (non-blocking) task on the microscope.
        
        The returned dictionary represents the task.
        """
        r = requests.post(self.base_uri+"/plugin/console/background_task", 
                          json={"code":code})
        r.raise_for_status()
        return r.json()
        
    def query_background_task(self, task):
        """Request the status of a background task"""
        r = requests.get(self.base_uri+"/task/{}/".format(task['id']))
        r.raise_for_status()
        return r.json()
    
    def run_background(self, code):
        """Start a long task on the microscope and block until it's finished.
        
        The return value is as for ``run_short``
        """
        t = self.start_background_task(code)
        r = {"status":"running"}
        while r["status"] == "running":
            time.sleep(0.1)
            r = self.query_background_task(t)
        return r
        
if __name__ == "__main__":
    con = MicroscopeConsole("localhost",62201)
    code = """
    response['test'] = "I worked!"
    """
    # run_background will start a background task in a thread on the server
    # and continuously poll it until the task finishes.
    t = con.run_background(code)
    print(t['return'])
    
    # run_short makes a blocking HTTP request and always returns the result.
    # it's best to save this for short queries only.
    print(con.run_short(code))