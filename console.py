import time
import numpy as np

from openflexure_microscope.plugins import MicroscopePlugin
from openflexure_microscope.utilities import set_properties
from openflexure_microscope.api.v1.views import MicroscopeViewPlugin
from openflexure_microscope.api.utilities import JsonPayload
from flask import request, jsonify, abort, current_app
import logging

ALLOWED_CLIENTS = ['127.0.0.1', 'localhost', '192.168.1.126', '192.168.1.106']

def client_ip(request):
    """Determine the IP address we're talking to"""
    client = request.environ.get('HTTP_X_FORWARDED_FOR')
    if client is None:
        client = request.environ.get('REMOTE_ADDR')
    return client

def restrict(request):
    if not current_app.debug:
        logging.error("The console plugin will only work in debug mode, this is an important security issue.")
        abort(403, "The console plugin only works when Flask is running in debug mode")
    if client_ip(request) not in ALLOWED_CLIENTS:
        logging.error("Someone tried to use a web console from {}, that's only allowed from localhost!".format(client_ip(request)))
        abort(403, "The console plugin can only be used from localhost for security reasons")


class ConsoleAPI(MicroscopeViewPlugin):
    def execute_json(self, payload):
        code = payload.param("code")
        return self.plugin.execute_string(code)

class ShortTaskAPI(ConsoleAPI):
    def post(self):
        restrict(request)
        payload = JsonPayload(request)
        return jsonify(self.execute_json(payload))


class BackgroundTaskAPI(ConsoleAPI):
    def post(self):
        restrict(request)
        payload = JsonPayload(request)
        task = self.microscope.task.start(self.execute_json, payload)

        # return a handle on the task
        return jsonify(task.state)


class ConsolePlugin(MicroscopePlugin):
    """
    Basic plugin to run code from a POST request
    """

    api_views = {
        '/short_task': ShortTaskAPI,
        '/background_task': BackgroundTaskAPI,
    }

    def execute_string(self, code_string):
        """Execute a string, which may access the microscope.

        NB there are deliberately very few security restrictions here.
        THIS IS NOT SUITABLE FOR PRODUCTION ENVIRONMENTS!!!!!
        """
        dummy_globals = {}
        response = {}
        logging.warn("Running user-supplied code via the console plug-in [DANGER]...")
        exec(code_string, dummy_globals, {'response':response, 'microscope':self.microscope})
        return response

