#!/bin/bash
################# This is an old version of the script! #################
# See the snippet of code on openflexure-microscope-server
ofport=62213
echo "Connecting to OpenFlexure.bath.ac.uk and opening a reverse tunnel"
echo "Using port $ofport on the server."
echo "if you need a password, enter 'openflexure'"
sudo systemctl start sshd
ssh microscope-revtunnel@openflexure.bath.ac.uk -N -R $ofport:localhost:22
